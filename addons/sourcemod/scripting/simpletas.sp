
#define DEBUG

#define PLUGIN_NAME					"SimpleTAS"
#define PLUGIN_AUTHOR				"GameChaos"
#define PLUGIN_DESCRIPTION			""
#define PLUGIN_VERSION				"0.0.2"
#define PLUGIN_URL					""

#define INT_MAX						~(1 << 31)

#define PLAYBACKSPEED_MULTIPLIER	2.0
#define PLAYBACKSPEED_MIN			0.125
#define PLAYBACKSPEED_MAX			16.0

#define REPLAY_EARLIEST_VERSION		102
#define REPLAY_FMT_VERSION			102
#define REPLAY_MAGIC_NUMBER			0x1337
#define REPLAY_PATH_SM				"data/simpletas"
#define REPLAY_SUFFIX				".simpletas"
#define REPLAY_MAX_FILES			64
#define REPLAY_NAME_LENGTH			64

#define DEFAULT_BOT_NAME		"[STAS]"

#define CHAT_PREFIX				"{default}[{olive}GC{default}]"

#define MAX_BEAM_TICKS			256

#define C_WHITE					{ 255, 255, 255, 255 }
#define C_RED					{ 255,   0,   0, 255 }
#define C_GREEN					{   0, 255,   0, 255 }
#define C_BLUE					{   0,   0, 255, 255 }
#define C_YELLOW				{ 255, 255,   0, 255 }

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <gamechaos>
#include <colors>

#undef REQUIRE_EXTENSIONS
#undef REQUIRE_PLUGIN
#include <GlobalAPI-Core>

#pragma semicolon 1
#pragma newdecls required

enum
{
	PLAYERSTATE_NOT_RECORDING,
	PLAYERSTATE_RECORDING,
	// paused states have to be below recording state
	PLAYERSTATE_PAUSED,
	PLAYERSTATE_REWIND,
	PLAYERSTATE_FASTFORWARD
};

// this has to match the ReplayTick enum struct
enum
{
	BLOCK_REPLAYFLAGS,
	BLOCK_RESERVED,
	BLOCK_ORIGIN[3],
	BLOCK_VELOCITY[3],
	BLOCK_ANGLES[3],
	BLOCK_FLAGS,
	BLOCK_BUTTONS,
	BLOCK_MOVETYPE,
	BLOCK_DUCKSPEED,
	BLOCK_DUCKAMOUNT,
	BLOCK_STAMINA,
	BLOCKSIZE
};

enum struct ReplayTick
{
	int replayFlags; // used for compression! only used when reading/writing. tracks which cells are saved.
	any reserved; // this is reserved for more replayflags in the future!
	float origin[3];
	float velocity[3];
	float angles[3];
	int flags;
	int buttons;
	MoveType moveType;
	float duckSpeed;
	float duckAmount;
	float stamina;
}

ArrayList g_alReplayData;
ArrayList g_alRecording[MAXPLAYERS + 1];
bool g_bLateLoad;
bool g_bShowBeam[MAXPLAYERS + 1];
float g_fPlaybackSpeed[MAXPLAYERS + 1];
Handle g_hHudSynchroniser[MAXPLAYERS + 1];
int g_iPlayerstate[MAXPLAYERS + 1];
int g_iCurrentTick[MAXPLAYERS + 1];
int g_iPauseTick[MAXPLAYERS + 1];

int g_iBeam;
bool g_bGlobalAPIAvailable;

// TODO:
// BUG: sometimes the plugin thinks that YOU are the bot.

public Plugin myinfo =
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

#include "simpletas/menus.sp"
#include "simpletas/replays.sp"
#include "simpletas/tastools.sp"
#include "simpletas/autostrafe.sp"

// =======
//  HOOKS
// =======

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	g_bLateLoad = late;
}

public void OnLibraryAdded(const char[] name)
{
	if (StrEqual(name, "GlobalAPI-Core"))
	{
		g_bGlobalAPIAvailable = true;
	}
}

public void OnLibraryRemoved(const char[] name)
{
	if (StrEqual(name, "GlobalAPI-Core"))
	{
		g_bGlobalAPIAvailable = false;
	}
}


public void OnPluginStart()
{
	OnPluginStart_Tastools();
	OnPluginStart_Autostrafe();
	
	RegConsoleCmd("sm_simpletas", Command_Simpletas);
	
	RegConsoleCmd("sm_stsave", Command_Savetas);
	
	RegConsoleCmd("sm_stpause", Command_Pause);
	RegConsoleCmd("sm_stprevpause", Command_Prevpause);
	RegConsoleCmd("sm_stspeed", Command_Speed);
	
	RegConsoleCmd("sm_stshowbeam", Command_Showpath);
	RegConsoleCmd("sm_stshowstats", Command_ShowStats, "Print stats of current tas into the console.");
	RegConsoleCmd("sm_stprintticks", Command_Printticks, "Print stats of 32 ticks around specified tick. Example: sm_stprintticks 128");
	
	RegConsoleCmd("+st_fastforward", Command_StartFastforward);
	RegConsoleCmd("-st_fastforward", Command_StopFastforward);
	RegConsoleCmd("+st_rewind", Command_StartRewind);
	RegConsoleCmd("-st_rewind", Command_StopRewind);

	HookEvent("round_start", Event_RoundStart);
	HookEvent("player_spawned", Event_PlayerSpawned, EventHookMode_Pre);

	if (g_bLateLoad)
	{
		FindBot();
		for (int client = 1; client <= MaxClients; client++)
		{
			if (GCIsValidClient(client))
			{
				OnClientPostAdminCheck(client);
			}
		}
	}
}

public void OnPluginEnd()
{
	OnPluginEnd_Replays();
}

public void OnMapStart()
{
	if (g_bGlobalAPIAvailable)
	{
		GlobalAPI_GetAuthStatus(GlobalAPI_OnGetAuthStatus);
	}
}

public int GlobalAPI_OnGetAuthStatus(bool bFailure, bool authenticated)
{
	if (authenticated)
	{
		SetFailState("You cannot use this plugin on globalised KZ servers!");
	}
}


public void OnConfigsExecuted()
{
	ChangeBotVars();
	
	g_iBeam = PrecacheModel("materials/sprites/laser.vmt", true);
}

public void OnClientPostAdminCheck(int client)
{
	OnClientPostAdminCheck_Tastools(client);
	OnClientPostAdminCheck_Autostrafe(client);
	ResetPlayerVars(client);
}

public void OnClientDisconnect(int client)
{
	OnClientDisconnect_Autostrafe(client);
}


public void Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	FindBot();
}

public Action Event_PlayerSpawned(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	return OnPlayerSpawned_Replays(client);
}

public void OnGameFrame()
{
	if (GetGameTickCount() % RoundFloat(1.0 / GetTickInterval()) == 0)
	{
		Timer_OneSecondLoop();
	}
}

public void Timer_OneSecondLoop()
{
	Timer_OneSecondLoop_Autostrafe();
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	OnPlayerRunCmd_Tastools(client, buttons);
	OnPlayerRunCmd_Autostrafe(client, buttons, impulse, vel, angles, weapon, subtype, cmdnum, tickcount, seed);
	OnPlayerRunCmd_Replays(client, buttons, vel, angles);
	
	return Plugin_Changed;
}

public void OnPlayerRunCmdPost(int client, int buttons, int impulse, const float vel[3], const float angles[3], int weapon, int subtype, int cmdnum, int tickcount, int seed, const int mouse[2])
{
	OnPlayerRunCmdPost_Tastools(client);
	
	if (!GCIsValidClient(client) || !IsPlayerAlive(client) || IsFakeClient(client))
	{
		return;
	}
	
	bool validRecording;
	validRecording = g_alRecording[client] && g_iCurrentTick[client] != -1;
	
	ReplayTick replayTick;
	if (validRecording)
	{
		g_alRecording[client].GetArray(g_iCurrentTick[client], replayTick);
	}
	
	// Replay beam
	if (g_bShowBeam[client]
		&& g_iPlayerstate[client] != PLAYERSTATE_NOT_RECORDING
		&& validRecording
		&& GetGameTickCount() % 128 == 0)
	{
		for (int i = g_iCurrentTick[client] - (MAX_BEAM_TICKS / 2);
			i < g_iCurrentTick[client] + (MAX_BEAM_TICKS / 2);
			i++)
		{
			if (i < 1)
			{
				continue;
			}
			
			if (i >= g_alRecording[client].Length)
			{
				break;
			}
			
			ReplayTick replayTickBeam;
			g_alRecording[client].GetArray(i, replayTickBeam);
			
			int colour[4];
			colour = (replayTickBeam.flags & FL_ONGROUND) ? C_RED : C_BLUE;
			
			if (replayTickBeam.moveType == MOVETYPE_LADDER)
			{
				colour = C_BLUE;
			}
			
			float lastOrigin[3];
			GCCopyArrayArrayIndex(g_alRecording[client], i - 1, lastOrigin, sizeof lastOrigin, BLOCK_ORIGIN);
			TE_SetupBeamPoints(
				.start = lastOrigin,
				.end = replayTickBeam.origin,
				.ModelIndex = g_iBeam,
				.HaloIndex = 0,
				.StartFrame = 0,
				.FrameRate = 0,
				.Life = 1.0,
				.Width = 1.0,
				.EndWidth = 1.0,
				.FadeLength = 0,
				.Amplitude = 0.0,
				.Color = colour,
				.Speed = 0);
			TE_SendToClient(client);
		}
	}
	
	if (g_iPlayerstate[client] == PLAYERSTATE_RECORDING)
	{
		RecordTAS(client);
	}
	else if (validRecording)
	{
		if (g_iPlayerstate[client] == PLAYERSTATE_PAUSED)
		{
			FreezePlayer(client, replayTick);
		}
		else if (g_iPlayerstate[client] == PLAYERSTATE_REWIND)
		{
			RewindTAS(client);
			FreezePlayer(client, replayTick);
		}
		else if (g_iPlayerstate[client] == PLAYERSTATE_FASTFORWARD)
		{
			FastforwardTAS(client);
			FreezePlayer(client, replayTick);
		}
	}
	
	if (g_hHudSynchroniser[client] != INVALID_HANDLE)
	{
		if (g_iPlayerstate[client] != PLAYERSTATE_NOT_RECORDING)
		{
			// show time
			char time[64];
			GCFormatTickTimeHHMMSS(g_iCurrentTick[client], 1.0 / GetTickInterval(), time, sizeof time);
			Format(time, sizeof time, "TAS Time:\n%i\n%s", g_iCurrentTick[client] + 1, time);
			
			SetHudTextParams(0.01, 0.3, 0.1, 255, 255, 255, 255, 0, 0.0, 0.0, 0.0);
			ShowSyncHudText(client, g_hHudSynchroniser[client], time);
		}
		else
		{
			ClearSyncHud(client, g_hHudSynchroniser[client]);
		}
	}
}

// ==========
//  COMMANDS
// ==========

public Action Command_Savetas(int client, int args)
{
	if (g_iPlayerstate[client] == PLAYERSTATE_NOT_RECORDING)
	{
		return;
	}
	
	char replayName[REPLAY_NAME_LENGTH];
	GetCmdArgString(replayName, sizeof(replayName));
	ReplaceString(replayName, sizeof(replayName), REPLAY_SUFFIX, "", false);
	
	char path[PLATFORM_MAX_PATH];
	if (SaveTAS(client, path, replayName))
	{
		CPrintToChat(client, "%s TAS has been saved to: {olive}\"%s\"{default}.", CHAT_PREFIX, path);
	}
}

public Action Command_Pause(int client, int args)
{
	if (!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	if (HandlePausing(client))
	{
		bool paused = g_iPlayerstate[client] == PLAYERSTATE_PAUSED;
		CPrintToChat(client, "%s Recording %s.", CHAT_PREFIX, paused ? "pause" : "resumed");
	}
	
	return Plugin_Handled;
}

public Action Command_Prevpause(int client, int args)
{
	if (!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	if (HandlePreviousPause(client))
	{
		bool paused = g_iPlayerstate[client] == PLAYERSTATE_PAUSED;
		CPrintToChat(client, "%s Recording %s.", CHAT_PREFIX, paused ? "pause" : "resumed");
	}
	
	return Plugin_Handled;
}

public Action Command_Speed(int client, int args)
{
	if (!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	IncrementPlaybackSpeed(client);
	
	return Plugin_Handled;
}

public Action Command_StartFastforward(int client, int args)
{
	if (!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iPlayerstate[client] == PLAYERSTATE_PAUSED)
	{
		g_iPlayerstate[client] = PLAYERSTATE_FASTFORWARD;
	}
	
	return Plugin_Handled;
}

public Action Command_StopFastforward(int client, int args)
{
	if (!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iPlayerstate[client] == PLAYERSTATE_FASTFORWARD)
	{
		g_iPlayerstate[client] = PLAYERSTATE_PAUSED;
	}
	
	return Plugin_Handled;
}

public Action Command_StartRewind(int client, int args)
{
	if (!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iPlayerstate[client] == PLAYERSTATE_PAUSED)
	{
		g_iPlayerstate[client] = PLAYERSTATE_REWIND;
	}
	
	return Plugin_Handled;
}

public Action Command_StopRewind(int client, int args)
{
	if (!IsPlayerAlive(client) || IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iPlayerstate[client] == PLAYERSTATE_REWIND)
	{
		g_iPlayerstate[client] = PLAYERSTATE_PAUSED;
	}
	
	return Plugin_Handled;
}

public Action Command_Simpletas(int client, int args)
{
	if (IsFakeClient(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iPlayerstate[client] == PLAYERSTATE_NOT_RECORDING)
	{
		Showmenu_Simpletas(client);
	}
	else
	{
		Showmenu_SimpletasRecording(client);
	}
	
	return Plugin_Handled;
}

public Action Command_Showpath(int client, int args)
{
	g_bShowBeam[client] = !g_bShowBeam[client];
}

public Action Command_ShowStats(int client, int args)
{
	if (g_iPlayerstate[client] == PLAYERSTATE_NOT_RECORDING)
	{
		CPrintToChat(client, "%s You can only use this command when recording!", CHAT_PREFIX);
		return Plugin_Handled;
	}
	
	if (g_alRecording[client] == null)
	{
		return Plugin_Handled;
	}
	
	if (g_alRecording[client].Length < 1)
	{
		return Plugin_Handled;
	}
	
	int length = g_alRecording[client].Length;
	int framesOnGround = 0;
	int framesInAir = 0;
	
	ReplayTick replayTick;
	
	char buffer[1024];
	for (int tick; tick < length; tick++)
	{
		g_alRecording[client].GetArray(tick, replayTick);
		
		if (replayTick.flags & FL_ONGROUND)
		{
			if (framesOnGround == 0)
			{
				Format(buffer, sizeof buffer, "%sFIA: %5i\n", buffer, framesInAir);
			}
			
			framesOnGround++;
			framesInAir = 0;
		}
		else if (replayTick.moveType != MOVETYPE_LADDER)
		{
			if (framesInAir == 0)
			{
				Format(buffer, sizeof buffer, "%sFOG: %5i\n", buffer, framesOnGround);
			}
			
			framesInAir++;
			framesOnGround = 0;
		}
	}
	
	PrintToConsole(client, buffer);
	
	return Plugin_Handled;
}

public Action Command_Printticks(int client, int args)
{
	if (g_iPlayerstate[client] == PLAYERSTATE_NOT_RECORDING)
	{
		CPrintToChat(client, "%s You can only use this command when recording!", CHAT_PREFIX);
		return Plugin_Handled;
	}
	
	if (g_alRecording[client] == null)
	{
		CPrintToChat(client, "%s ERROR: Can't print anything! Invalid recording!", CHAT_PREFIX);
		return Plugin_Handled;
	}
	
	if (g_alRecording[client].Length < 1)
	{
		CPrintToChat(client, "%s ERROR: Can't print anything! Recording is 1 tick long!", CHAT_PREFIX);
		return Plugin_Handled;
	}
	
	if (GetCmdArgs() != 1)
	{
		CPrintToChat(client, "%s ERROR: Invalid number of arguments passed! Usage: !printticks <tick>. To easily access the end of the replay use negative indices: !printticks -32 prints ticks that are 32 ticks before the end.", CHAT_PREFIX);
		return Plugin_Handled;
	}
	
	char szBaseTick[32];
	GetCmdArg(1, szBaseTick, sizeof szBaseTick);
	int baseTick = StringToInt(szBaseTick);
	
	int replayLength = g_alRecording[client].Length;
	if (baseTick < 0)
	{
		baseTick = replayLength + baseTick;
	}
	
	ReplayTick replayTick;
	
	const int printTickCount = 32;
	int startTick = GCIntMax(0, baseTick - printTickCount);
	int endTick = GCIntMin(replayLength, startTick + printTickCount);
	
	char buffer[2048];
	for (int tick = startTick; tick < endTick; tick++)
	{
		g_alRecording[client].GetArray(tick, replayTick);
		
		int buttons = replayTick.buttons;
		
		PrintToConsole(client,
			"%s[%9i] Pos: %8.2f %8.2f %8.2f | Ang: %8.2f %8.2f | Buttons: %c%c%c%c %c%c %c",
			buffer,
			tick + 1,
			replayTick.origin[0], replayTick.origin[1], replayTick.origin[2],
			replayTick.angles[0], replayTick.angles[1],
			(buttons & IN_FORWARD)   ? 'W' : '_',
			(buttons & IN_MOVELEFT)  ? 'A' : '_',
			(buttons & IN_BACK)      ? 'S' : '_',
			(buttons & IN_MOVERIGHT) ? 'D' : '_',
			(buttons & IN_DUCK)      ? 'C' : '_',
			(buttons & IN_JUMP)      ? 'J' : '_',
			(buttons & IN_USE)       ? 'U' : '_');
	}
	
	return Plugin_Handled;
}

// ===========
//  FUNCTIONS
// ===========

void RecordTAS(int client)
{
	// recording initialisation
	if (g_iCurrentTick[client] == -1)
	{
		ResetRecording(client);
	}
	g_iCurrentTick[client]++;
	
	// check for rewind
	if (g_iCurrentTick[client] + 1 < g_alRecording[client].Length) //g_iTickCount[client])
	{
		//reset the recording array list
		g_alRecording[client].Resize(g_iCurrentTick[client]);
	}
	
	// record movement
	ReplayTick replayTick;
	
	GetClientAbsOrigin(client, replayTick.origin);
	GCGetClientVelocity(client, replayTick.velocity);
	GetClientEyeAngles(client, replayTick.angles);
	
	replayTick.flags = GetEntityFlags(client);
	replayTick.buttons = GetClientButtons(client);
	replayTick.moveType = GetEntityMoveType(client);
	replayTick.duckSpeed = GCGetClientDuckSpeed(client);
	replayTick.duckAmount = GCGetClientDuckAmount(client);
	replayTick.stamina = GetEntPropFloat(client, Prop_Send, "m_flStamina");
	
	g_alRecording[client].PushArray(replayTick);
}

// freeze the player.
void FreezePlayer(int client, const ReplayTick replayTick)
{
	TeleportEntity(client, replayTick.origin, replayTick.angles, replayTick.velocity);
	SetEntityFlags(client, replayTick.flags);
	GCSetClientDuckSpeed(client, replayTick.duckSpeed);
	GCSetClientDuckAmount(client, replayTick.duckAmount);
	SetEntPropFloat(client, Prop_Send, "m_flStamina", replayTick.stamina);
	SetEntityMoveType(client, MOVETYPE_NONE);
}

void IncrementPlaybackSpeed(int client)
{
	if (g_fPlaybackSpeed[client] * PLAYBACKSPEED_MULTIPLIER <= PLAYBACKSPEED_MAX)
	{
		g_fPlaybackSpeed[client] *= PLAYBACKSPEED_MULTIPLIER;
	}
	else
	{
		g_fPlaybackSpeed[client] = PLAYBACKSPEED_MIN;
	}
}

// return - changed value or not
bool HandlePausing(int client)
{
	if (g_iPlayerstate[client] == PLAYERSTATE_PAUSED)
	{
		// TODO: simplify this with enums and copyarrayarray etc.
		ReplayTick replayTick;
		g_alRecording[client].GetArray(g_iCurrentTick[client], replayTick);
		g_iPlayerstate[client] = PLAYERSTATE_RECORDING;
		g_iPauseTick[client] = g_iCurrentTick[client];
		SetEntityMoveType(client, replayTick.moveType);
		return true;
	}
	else if (g_iPlayerstate[client] != PLAYERSTATE_NOT_RECORDING)
	{
		g_iPlayerstate[client] = PLAYERSTATE_PAUSED;
		return true;
	}
	return false;
}

// return - changed value or not
bool HandlePreviousPause(int client)
{
	if (g_iPlayerstate[client] == PLAYERSTATE_NOT_RECORDING
		|| g_iPauseTick[client] < 0)
	{
		return false;
	}
	
	g_iPlayerstate[client] = PLAYERSTATE_PAUSED;
	g_iCurrentTick[client] = g_iPauseTick[client];
	
	return true;
}

void RewindTAS(int client)
{
	if (g_iCurrentTick[client] > 0)
	{
		if (g_fPlaybackSpeed[client] >= 1.0)
		{
			g_iCurrentTick[client] -= RoundFloat(g_fPlaybackSpeed[client]);
		}
		else
		{
			int iRepeatTicks = RoundFloat(1.0 / g_fPlaybackSpeed[client]);
			if (!(GetGameTickCount() % iRepeatTicks))
			{
				g_iCurrentTick[client]--;
			}
		}
		if (g_iCurrentTick[client] < 0)
		{
			g_iCurrentTick[client] = 0;
		}
	}
}

void FastforwardTAS(int client)
{
	if (g_iCurrentTick[client] < g_alRecording[client].Length - 1)
	{
		if (g_fPlaybackSpeed[client] >= 1.0)
		{
			g_iCurrentTick[client] += RoundFloat(g_fPlaybackSpeed[client]);
		}
		else
		{
			int iRepeatTicks = RoundFloat(1.0 / g_fPlaybackSpeed[client]);
			if (!(GetGameTickCount() % iRepeatTicks))
			{
				g_iCurrentTick[client]++;
			}
		}
		if (g_iCurrentTick[client] > g_alRecording[client].Length - 1)
		{
			g_iCurrentTick[client] = g_alRecording[client].Length - 1;
		}
	}
}

void ResetRecording(int client)
{
	if (g_alRecording[client] == null)
	{
		g_alRecording[client] = new ArrayList(BLOCKSIZE);
	}
	else
	{
		g_alRecording[client].Clear();
	}
	g_iCurrentTick[client] = -1;
	// set pause tick to 0 by default, so we can go to the start right away.
	g_iPauseTick[client] = 0;
}

void ResetPlayerVars(int client)
{
	g_fPlaybackSpeed[client] = 1.0;
	g_iPlayerstate[client] = PLAYERSTATE_NOT_RECORDING;
	g_iCurrentTick[client] = -1;
	g_iPauseTick[client] = -1;
	g_bShowBeam[client] = false;
	
	if (g_hHudSynchroniser[client] != INVALID_HANDLE)
	{
		delete g_hHudSynchroniser[client];
	}
	g_hHudSynchroniser[client] = CreateHudSynchronizer();
}

// bot things
void ChangeBotVars()
{
	FindConVar("mp_autoteambalance").BoolValue = false;
	FindConVar("mp_limitteams").IntValue = 0;
	FindConVar("bot_stop").BoolValue = true;
	FindConVar("bot_chatter").SetString("off");
	FindConVar("bot_zombie").BoolValue = true;
	FindConVar("bot_join_after_player").BoolValue = false;
	//FindConVar("bot_quota_mode").SetString("normal");
	FindConVar("bot_quota").IntValue = 1;
	//FindConVar("bot_mimic").IntValue = INT_MAX;	// such a large entity hopefully doesn't exist so it won't mimic that
	FindConVar("bot_quota_mode").SetString("normal");
}