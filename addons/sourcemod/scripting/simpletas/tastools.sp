
// TAS plugin helper

static bool s_bAutojump[MAXPLAYERS + 1];
static bool s_bAutoLowjump[MAXPLAYERS + 1];
static bool s_bAutoCrouchjump[MAXPLAYERS + 1];
static bool s_bAutoTickperfectCrouchjump[MAXPLAYERS + 1];
static bool s_bAutoSbj[MAXPLAYERS + 1];
static bool s_bAutoJumpbug[MAXPLAYERS + 1];
static bool s_bAutoDuckbug[MAXPLAYERS + 1];

static int s_iFramesOnGround[MAXPLAYERS + 1];

void OnPluginStart_Tastools()
{
	RegConsoleCmd("+tas_autojump", Command_StartTasAutojump);
	RegConsoleCmd("-tas_autojump", Command_StopTasAutojump);
	
	RegConsoleCmd("+tas_autolowjump", Command_StartTasAutolowjump);
	RegConsoleCmd("-tas_autolowjump", Command_StopTasAutolowjump);
	
	RegConsoleCmd("+tas_autocrouchjump", Command_StartTasAutocrouchjump);
	RegConsoleCmd("-tas_autocrouchjump", Command_StopTasAutocrouchjump);
	
	RegConsoleCmd("+tas_autotickperfectcrouchjump", Command_StartTasAutotickperfectcrouchjump);
	RegConsoleCmd("-tas_autotickperfectcrouchjump", Command_StopTasAutotickperfectcrouchjump);
	
	RegConsoleCmd("+tas_autosbj", Command_StartTasAutosbj);
	RegConsoleCmd("-tas_autosbj", Command_StopTasAutosbj);
	
	RegConsoleCmd("+tas_autojumpbug", Command_StartTasAutojumpbug);
	RegConsoleCmd("-tas_autojumpbug", Command_StopTasAutojumpbug);
	
	RegConsoleCmd("tas_crouchjump", Command_TasCrouchjump);
	RegConsoleCmd("tas_lowjump", Command_TasLowjump);

	RegConsoleCmd("+tas_autoduckbug", Command_StartTasAutoduckbug);
	RegConsoleCmd("-tas_autoduckbug", Command_StopTasAutoduckbug);
}

void OnClientPostAdminCheck_Tastools(int client)
{
	Tastools_ResetVariables(client);
}

void OnPlayerRunCmd_Tastools(int client, int &buttons)
{
	if (!GCIsValidClient(client, true))
	{
		return;
	}
	
	Autojump(client, buttons);
	AutoLowjump(client);
	AutoCrouchjump(client);
	AutoTickperfectCrouchjump(client);
	AutoSbj(client, buttons);
	AutoJumpbug(client, buttons);
	AutoDuckbug(client, buttons);
}

void OnPlayerRunCmdPost_Tastools(int client)
{
	if (!GCIsValidClient(client, true))
	{
		return;
	}
	
	if (GetEntityFlags(client) & FL_ONGROUND)
	{
		s_iFramesOnGround[client]++;
	}
	else
	{
		s_iFramesOnGround[client] = 0;
	}
	/*{
	if (s_iFramesOnGround[client] == 1)
	{
		FindDistanceFromGround(client);
		s_iLandFIA[client] = s_iFramesInAir[client];
		PrintToChat(client, "FIA: %i", s_iLandFIA[client]);
		PrintToConsole(client, "FIA: %i", s_iLandFIA[client]);
	}
	
	if (~GetEntityFlags(client) & FL_ONGROUND
		&& GetEntityMoveType(client) == MOVETYPE_WALK)
	{
		s_iFramesInAir[client]++;
	}
	else
	{
		s_iFramesInAir[client] = 0;
	}
	
	FindHeight(client);
	
	float fPosition[3];
	GetClientAbsOrigin(client, fPosition);
	if (s_bJump[client])
	{
		if (s_fMaxHeight[client] < fPosition[2])
		{
			s_fMaxHeight[client] = fPosition[2];
		}
	}
	
	if (s_bJump[client] && GetEntityFlags(client) & FL_ONGROUND)
	{
		//PrintToChat(client, "height %f", s_fMaxHeight[client] - s_fJumpPosition[client][2]);
		s_bJump[client] = false;
	}
	
	SetHudTextParams(-1.0, -1.0, 0.02, 255, 255, 255, 255, 0, 0.0, 0.0, 0.0);
	
	ShowHudText(client, -1, "FIA: %i\nFOG: %i\nDFG: %.4f\nHEIGHT: %.4f", s_iLandFIA[client], s_iJumpFOG[client], s_fDistanceFromGround[client], s_fHeight[client]);
	}*/
}

public Action Command_StartTasAutojump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutojump[client] = true;
	
	return Plugin_Handled;
}

public Action Command_StopTasAutojump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutojump[client] = false;
	
	return Plugin_Handled;
}


public Action Command_StartTasAutolowjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoLowjump[client] = true;
	
	return Plugin_Handled;
}

public Action Command_StopTasAutolowjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoLowjump[client] = false;
	
	return Plugin_Handled;
}


public Action Command_StartTasAutocrouchjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoCrouchjump[client] = true;
	
	return Plugin_Handled;
}

public Action Command_StopTasAutocrouchjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoCrouchjump[client] = false;
	
	return Plugin_Handled;
}


public Action Command_StartTasAutotickperfectcrouchjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoTickperfectCrouchjump[client] = true;
	
	return Plugin_Handled;
}

public Action Command_StopTasAutotickperfectcrouchjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoTickperfectCrouchjump[client] = false;
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~(IN_DUCK));
	
	return Plugin_Handled;
}


public Action Command_StartTasAutosbj(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoSbj[client] = true;
	
	return Plugin_Handled;
}

public Action Command_StopTasAutosbj(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoSbj[client] = false;
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_DUCK);
	
	return Plugin_Handled;
}


public Action Command_StartTasAutojumpbug(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoJumpbug[client] = true;
	
	return Plugin_Handled;
}



public Action Command_StopTasAutojumpbug(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoJumpbug[client] = false;
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_DUCK);
	
	return Plugin_Handled;
}


public Action Command_StartTasAutoduckbug(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoDuckbug[client] = true;
	
	return Plugin_Handled;
}


public Action Command_StopTasAutoduckbug(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	s_bAutoDuckbug[client] = false;
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_DUCK);
	
	return Plugin_Handled;
}


public Action Command_TasCrouchjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	ExecuteCrouchjump(client);
	
	return Plugin_Handled;
}

public Action Command_TasLowjump(int client, int args)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Handled;
	}
	
	ExecuteLowjump(client);
	
	return Plugin_Handled;
}

static void Autojump(int client, int &buttons)
{
	if (!s_bAutojump[client])
	{
		return;
	}
	
	if (s_iFramesOnGround[client])
	{
		buttons |= IN_JUMP;
	}
}

static void AutoLowjump(int client)
{
	if (!s_bAutoLowjump[client])
	{
		return;
	}
	
	if (s_iFramesOnGround[client])
	{
		ExecuteLowjump(client);
		// reset so it doesn't re-execute every tick
		s_bAutoLowjump[client] = false;
	}
}

static void AutoCrouchjump(int client)
{
	if (!s_bAutoCrouchjump[client])
	{
		return;
	}
	
	if (s_iFramesOnGround[client])
	{
		ExecuteCrouchjump(client);
		// reset so it doesn't re-execute every tick
		s_bAutoCrouchjump[client] = false;
	}
}

static void AutoTickperfectCrouchjump(int client)
{
	if (!s_bAutoTickperfectCrouchjump[client])
	{
		return;
	}
	
	if (s_iFramesOnGround[client])
	{
		ExecuteTickperfectCrouchjump(client);
		// reset so it doesn't re-execute every tick
		s_bAutoTickperfectCrouchjump[client] = false;
	}
}

static void AutoSbj(int client, int &buttons)
{
	if (!s_bAutoSbj[client])
	{
		return;
	}
	
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_DUCK);
	
	if (s_iFramesOnGround[client])
	{
		ExecuteSbj(client, buttons);
		// reset so it doesn't re-execute every tick
		s_bAutoSbj[client] = false;
	}
}

static void AutoJumpbug(int client, int &buttons)
{
	if (!s_bAutoJumpbug[client])
	{
		return;
	}
	
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_DUCK);
	
	float origin[3];
	GetClientAbsOrigin(client, origin);
	origin[2] -= 9.0;
	
	float result[3];
	if (GCTraceGround(client, origin, result))
	{
		ExecuteJumpbug(client, buttons);
		// reset so it doesn't re-execute every tick
		s_bAutoJumpbug[client] = false;
	}
}

static void AutoDuckbug(int client, int &buttons)
{
	if (!s_bAutoDuckbug[client])
	{
		return;
	}
	
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_DUCK);

	float origin[3];
	GetClientAbsOrigin(client, origin);
	origin[2] -= 9.0;
	
	float result[3];
	if (GCTraceGround(client, origin, result))
	{
		GCSetClientForcedButtons(client, (GCGetClientForcedButtons(client) & ~IN_DUCK));
		// reset so it doesn't re-execute every tick
		s_bAutoDuckbug[client] = false;
	}
}


static void ExecuteLowjump(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_DUCK);
	RequestFrame(Requestframe_ExecuteLowjump, client);
}

public void Requestframe_ExecuteLowjump(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_JUMP);
	RequestFrame(Requestframe_ExecuteLowjump2, client);
}

public void Requestframe_ExecuteLowjump2(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_JUMP);
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_DUCK);
}


static void ExecuteCrouchjump(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_DUCK);
	RequestFrame(Requestframe_ExecuteCrouchjump, client);
}

public void Requestframe_ExecuteCrouchjump(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_JUMP);
	RequestFrame(Requestframe_ExecuteCrouchjump2, client);
}

public void Requestframe_ExecuteCrouchjump2(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_JUMP);
	RequestFrame(Requestframe_ExecuteCrouchjump3, client);
}

public void Requestframe_ExecuteCrouchjump3(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_DUCK);
}


static void ExecuteTickperfectCrouchjump(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) | IN_DUCK | IN_JUMP);
	RequestFrame(Requestframe_ExecuteTickperfectCrouchjump, client);
}

public void Requestframe_ExecuteTickperfectCrouchjump(int client)
{
	RequestFrame(Requestframe_ExecuteTickperfectCrouchjump2, client);
}

public void Requestframe_ExecuteTickperfectCrouchjump2(int client)
{
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_JUMP);
}


static void ExecuteSbj(int client, int &buttons)
{
	//buttons &= ~IN_DUCK;
	GCSetClientForcedButtons(client, GCGetClientForcedButtons(client) & ~IN_DUCK | IN_JUMP);
	RequestFrame(Requestframe_ExecuteSbj, client);
}

public void Requestframe_ExecuteSbj(int client)
{
	GCSetClientForcedButtons(client, (GCGetClientForcedButtons(client) & ~(IN_JUMP)) | IN_DUCK);
	RequestFrame(Requestframe_ExecuteSbj2, client);
}

public void Requestframe_ExecuteSbj2(int client)
{
	GCSetClientForcedButtons(client, (GCGetClientForcedButtons(client) & ~IN_DUCK));
}


static void ExecuteJumpbug(int client, int &buttons)
{
	GCSetClientForcedButtons(client, (GCGetClientForcedButtons(client) & ~IN_DUCK) | IN_JUMP);
	RequestFrame(Requestframe_ExecuteJumpbug, client);
}

public void Requestframe_ExecuteJumpbug(int client)
{
	GCSetClientForcedButtons(client, (GCGetClientForcedButtons(client) & ~IN_JUMP));
}

static void Tastools_ResetVariables(int client)
{
	s_bAutojump[client] = false;
	s_bAutoLowjump[client] = false;
	s_bAutoCrouchjump[client] = false;
}