
#define SIDESPEED                     450.0
#define NON_JUMP_VELOCITY             140.0
#define CS_PLAYER_SPEED_WALK_MODIFIER 0.52
#define GOKZ_AIR_MAX_WISHSPEED        30.0

static bool g_bEnabled[MAXPLAYERS + 1];
static ConVar g_cvAiraccelerate;
static ConVar g_cvAirMaxWishspeed;
static float g_fLastAiraccelerate[MAXPLAYERS + 1];
static float g_fLastAirMaxWishspeed[MAXPLAYERS + 1];
static float g_fSidespeed[MAXPLAYERS + 1];

// gamedata
int g_iOffset_CBasePlayer_m_surfaceFriction;
static Handle g_hGetPlayerMaxSpeed;

void OnPluginStart_Autostrafe()
{
	GameData gameData;
	gameData = LoadGameConfigFile("simpletas.games");
	
	if (gameData == INVALID_HANDLE)
	{
		SetFailState("Couldn't load gamedata simpletas.games.txt");
	}
	
	// GetPlayerMaxSpeed
	StartPrepSDKCall(SDKCall_Player);
	if (!PrepSDKCall_SetFromConf(gameData, SDKConf_Virtual, "GetPlayerMaxSpeed"))
	{
		SetFailState("Couldn't find GetPlayerMaxSpeed in simpletas.games.txt");
	}
	PrepSDKCall_SetReturnInfo(SDKType_Float, SDKPass_ByValue);
	g_hGetPlayerMaxSpeed = EndPrepSDKCall();
	
	if (g_hGetPlayerMaxSpeed == INVALID_HANDLE)
	{
		SetFailState("Failed to create sdkcall g_hGetPlayerMaxSpeed");
	}
	
	// m_surfaceFriction
	char buffer[32];
	if (!gameData.GetKeyValue("CBasePlayer::m_surfaceFriction", buffer, sizeof buffer))
	{
		SetFailState("Couldn't find CBasePlayer::m_surfaceFriction offset in simpletas.games.txt");
	}
	g_iOffset_CBasePlayer_m_surfaceFriction = StringToInt(buffer);
	
	int offset = FindSendPropInfo("CBasePlayer", "m_ubEFNoInterpParity");
	if (offset < 1)
	{
		SetFailState("Couldn't find m_ubEFNoInterpParity offset with FindSendPropInfo");
	}
	
	g_iOffset_CBasePlayer_m_surfaceFriction = offset - g_iOffset_CBasePlayer_m_surfaceFriction;
	
	delete gameData;
	
	//
	
	RegAdminCmd("sm_autostrafe", Command_Autostrafe, ADMFLAG_GENERIC, "Toggles autostrafe.");
	
	g_cvAiraccelerate = FindConVar("sv_airaccelerate");
	g_cvAirMaxWishspeed = FindConVar("sv_air_max_wishspeed");
}

void OnClientPostAdminCheck_Autostrafe(int client)
{
	// reset to defaults
	g_bEnabled[client] = false;
	g_fLastAiraccelerate[client] = 100.0;
	g_fLastAirMaxWishspeed[client] = 30.0;
	g_fSidespeed[client] = SIDESPEED;
	
	// get cl_sidespeed value
	QueryClientConVar(client, "cl_sidespeed", QueryCVar_ClSidespeed);
	
	SDKHook(client, SDKHook_PreThinkPost, SDKHookPreThinkPost);
}

void OnClientDisconnect_Autostrafe(int client)
{
	SDKUnhook(client, SDKHook_PreThinkPost, SDKHookPreThinkPost);
}

void SDKHookPreThinkPost(int client)
{
	// get real convar values right before movement gets processed.
	g_fLastAiraccelerate[client] = g_cvAiraccelerate.FloatValue;
	g_fLastAirMaxWishspeed[client] = g_cvAirMaxWishspeed.FloatValue;
}

void Timer_OneSecondLoop_Autostrafe()
{
	for (int client = 1; client <= MaxClients; client++)
	{
		if (GCIsValidClient(client))
		{
			QueryClientConVar(client, "cl_sidespeed", QueryCVar_ClSidespeed);
		}
	}
}

void QueryCVar_ClSidespeed(QueryCookie cookie, int client, ConVarQueryResult result, const char[] cvarName, const char[] cvarValue)
{
	if (result == ConVarQuery_Okay)
	{
		g_fSidespeed[client] = StringToFloat(cvarValue);
	}
}

Action OnPlayerRunCmd_Autostrafe(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed)
{
	if (!GCIsValidClient(client, true))
	{
		return Plugin_Continue;
	}
	
	if (g_bEnabled[client]
		&& !(GetEntityFlags(client) & FL_ONGROUND)
		&& GetEntityMoveType(client) == MOVETYPE_WALK
		&& !(buttons & (IN_MOVELEFT | IN_MOVERIGHT | IN_FORWARD | IN_BACK)))
	{
		float velocity[3];
		GCGetClientVelocity(client, velocity);
		
		float maxspeed = CalculateMaxspeed(client, buttons, velocity);
		float airaccelerate = g_fLastAiraccelerate[client];
		float maxWishspeed = g_fLastAirMaxWishspeed[client];
		
		float optimalAngle = CalcOptimalWishdirYaw(maxWishspeed, airaccelerate, maxspeed, GetPlayerSurfaceFriction(client), velocity);
		
		vel[0] = 0.0;
		vel[1] = SIDESPEED;
		
		float velocityYaw = RadToDeg(ArcTangent2(velocity[1], velocity[0]));
		float diffAngle = GCNormaliseYaw(angles[1] - velocityYaw);
		bool strafingRight = diffAngle > 0.0;
		
		float optimalAngleDeg = 90.0 - RadToDeg(optimalAngle);
		
		if (strafingRight)
		{
			vel[1] = -vel[1];
			optimalAngleDeg = -optimalAngleDeg;
		}
		
		angles[1] = velocityYaw + optimalAngleDeg;
	}
	
	return Plugin_Changed;
}

// private

static Action Command_Autostrafe(int client, int args)
{
	if (client <= 0)
	{
		PrintToServer("[sm_autostrafe] Cannot call this command from the console!");
		return Plugin_Handled;
	}
	
	g_bEnabled[client] = !g_bEnabled[client];
	
	PrintToChat(client, "[SM] Autostrafe: %s", (g_bEnabled[client] ? "ON" : "OFF"));
	
	return Plugin_Handled;
}

static float GetPlayerMaxSpeed(int client)
{
	return SDKCall(g_hGetPlayerMaxSpeed, client);
}

static float GetPlayerSurfaceFriction(int client)
{
	return view_as<float>(GetEntDataFloat(client, g_iOffset_CBasePlayer_m_surfaceFriction));
}

static float CalculateMaxspeed(int client, int buttons, const float velocity[3])
{
	float maxspeed = GetPlayerMaxSpeed(client);
	
	// walking speed reduction (NOT DUCKING WHATSOEVER, ONLY WALKING)
	if (~buttons & IN_DUCK
		&& !GetEntProp(client, Prop_Send, "m_bDucking")
		&& ~GetEntityFlags(client) & FL_DUCKING
		&& buttons & IN_SPEED)
	{
		float adjustedmaxspeed = maxspeed * CS_PLAYER_SPEED_WALK_MODIFIER;
		
		if (adjustedmaxspeed + 25.0 > GetVectorLength(velocity))
		{
			maxspeed = adjustedmaxspeed;
		}
	}
	
	// stamina speed reduction
	float stamina = GCGetClientStamina(client);
	
	if (stamina != 0.0)
	{
		stamina += GetTickInterval() * FindConVar("sv_staminarecoveryrate").FloatValue;
	}
	
	if (stamina > 0.0)
	{
		float multiplier = 1.0 - stamina * 0.01;
		
		if (multiplier >= 0.0)
		{
			multiplier = GCFloatMin(multiplier, 1.0);
		}
		else
		{
			multiplier = 0.0;
		}
		
		maxspeed *= (multiplier * multiplier);
	}
	
	// TODO: first tick in air actually uses m_flVelocityModifier? FIX!
//	if (GetEntityFlags(client) & FL_ONGROUND)
//	{
//		maxspeed *= GetEntPropFloat(client, Prop_Send, "m_flVelocityModifier");
//	}
	
	// duck speed crop
	if (GetEntityFlags(client) & FL_DUCKING)
	{
		maxspeed *= 0.34;
	}
	
	return maxspeed;
}

// Credits: https://github.com/HLTAS/hlstrafe
static float CalcOptimalWishdirYaw(float airMaxWishspeed, float airaccelerate, float groundMaxspeed, float surfaceFriction, const float velocity[3])
{
	float horizontalSpeed = GCGetVectorLength2D(velocity);
	float accelspeed = airaccelerate * groundMaxspeed * GetTickInterval() * surfaceFriction;
	
	float result = 0.0;
	if (accelspeed <= 0.0)
	{
		result = GC_RADIANS(180.0);
	}
	else if (horizontalSpeed > 0.0)
	{
		float tmp = airMaxWishspeed - accelspeed;
		if (tmp <= 0.0)
		{
			result = GC_RADIANS(90.0);
		}
		else if (tmp < horizontalSpeed)
		{
			result = ArcCosine(tmp / horizontalSpeed);
		}
	}
	
	return result;
}
