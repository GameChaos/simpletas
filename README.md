# SimpleTAS

WIP TAS plugin for csgo and sourcemod.

This plugin is intended to only be used on LAN servers for now.

# Guidelines for making a proper TAS

If you want to make a TAS that doesn't break any gameplay "rules", then use these guidelines.

By breaking gameplay rules I mean very specific things like exploiting a bug in the STAS plugin, or changing a convar to your advantage.

Everything that's possible to modify during normal gameplay should be able to be modified the same way with a TAS. Basically the only things that can be modified to affect the player's movement are eye angles and movement keys (wasd, jump etc). BLA BLA BLA BLA BLA BLA BLA.

- Don't use sv_autobunnyhopping. Use +tas_autojump, though WARNING: if you have any anticheat plugins on your server, then this might get picked up by them, so remove them before using this.

TODO: fill this out LOL!

Current issues:

Prestrafing variables aren't being tracked, so using prekeeping in tases or whatever doesn't work properly/you can exploit it with pausing and rewind.

# Commands

| Command                       | Description |
|-------------------------------|-------------|
| `!simpletas`                  | Open the SimpleTAS menu. |
| `!strespawnbot / !st_respawn `| Respawn the STAS replay bot if it's stuck somewhere for some reason. |
| `!strespawn`                  |             |
|                               |             |
| `!stsave <optional name>`     | Save the current recording with an optional name (this won't close your menu). If no name has been provided, then the naming convention is <mapname>_<tickcount>.replay|
|                               |             |
| `!stpause`                    | Pause the current recording. |
| `!stprevpause`                | Jump to the previous pause. |
| `!stspeed`                    | Change the fast-forward/rewind speed.|
|                               |             |
| `!stshowbeam`                 | Show a neat position beam of the previous ticks. |
| `!stshowstats`                | Print possibly useful stats of current tas into the console. Useful for hardcore optimisation. |
| `!stprintticks [tick number]`               | Print stats of 32 ticks around a specified tick. Use negative numbers to look at the end of the replay. |
|                               |             |
| `+st_fastforward`             | Fast-forward the recording. |
| `+st_rewind`                  | Rewind the recording |

# Tools:

| Command | Description |
|---------|-------------|
| `!autostrafe` | Get perfect speed gain and distance in a striaght line. |
| `+tas_autojump` | Correct autobhop (not the incorrect, bad sv_autobunnyhopping!). WARNING: if you have any anticheat plugins on your server, then this might get picked up by them, so remove them before using this. |
| `+tas_autolowjump` | Performs an automatic perfect lowjump when you land on the ground or already are on the ground.|
| `+tas_autocrouchjump` | Performs an automatic perfect crouchjump when you land on the ground or already are on the ground.|
| `+tas_autotickperfectcrouchjump` | Only useable for VNL tases. Performs a tick perfect crouchjump when you land etc etc.|
| `+tas_autosbj` | "Crouchbhop" up sequential 56-64 height blocks basically.|
| `+tas_autoduckbug` | Automatically does a duckbug. Useful for ignoring falling stamina in vanilla KZ TASes.|
| `+tas_autojumpbug` | Automatically does a duckbug with a jump, preserving speed and height in vanilla KZ.|
| `tas_crouchjump` | Does a crouchjump instantly when you press the key.|
| `tas_lowjump` | Does a lowjump instantly when you press the key.|



# TODO:

- Fix targetname exploits.
- Make !stprevpause go to the start of the replay if replay hasn't been paused yet.